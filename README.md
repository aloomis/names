# Name Analysis

Analyze baby name frequency from Social Security Administration data.

1. Download [namesbystate.zip][1] from [Beyond the Top 1000 Names][2].
2. Extract the raw names data `mkdir data; unzip namesbystate.zip -d data`.
3. Build the names database `names.py create data`.
4. Query! See all the options with `names.py query --help`.

[1]: https://www.ssa.gov/oact/babynames/state/namesbystate.zip
[2]: https://www.ssa.gov/oact/babynames/limits.html
