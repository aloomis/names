#!/usr/bin/env python

import argparse
import itertools
import os
import sqlite3


DATABASE_NAME = "names.db"

class _WhereClause(object):
    def __init__(self):
        self._clause = ""
        self._args = []

    def and_in(self, col, values):
        if not values:
            return
        self._clause += " WHERE" if not self._clause else " AND"
        self._clause += " {} in ({})".format(col, ",".join(["?"]*len(values)))
        self._args += values

    def and_like(self, col, values):
        if not values:
            return
        likes = ["{} like ?".format(col)]*len(values)
        self._clause += " WHERE" if not self._clause else " AND"
        self._clause += " ({})".format(" OR ".join(likes))
        self._args += values

    @property
    def clause(self):
        return self._clause

    @property
    def args(self):
        return self._args

def _grouper(size, iterable):
    iterable_copy = iter(iterable)
    while True:
        group = list(itertools.islice(iterable_copy, size))
        if not group:
            return
        yield group

def create_table(args):
    with sqlite3.connect(DATABASE_NAME) as conn:
        cursor = conn.cursor()
        cursor.executescript(
            "DROP TABLE IF EXISTS namesbystate;"
            "CREATE TABLE namesbystate"
            "(state text, sex text, year int, name text, count int);")

        def _insert_into_table(data_file):
            # Inserts a single file into the database 1000 entries at a time.
            for group in _grouper(1000, data_file):
                entries = [tuple(line.split(",")) for line in group]
                cursor.executemany(
                    "INSERT INTO namesbystate VALUES (?,?,?,?,?)", entries)

        for filename in os.listdir(args.data_dir):
            if not filename.endswith(".TXT"):
                continue
            with open(os.path.join(args.data_dir, filename)) as data_file:
                print("Loading {}... ".format(filename), end="", flush=True)
                _insert_into_table(data_file)
                print("done")

def query_names(args):
    with sqlite3.connect(DATABASE_NAME) as conn:
        where = _WhereClause()
        where.and_in("state", args.states)
        where.and_in("sex",   args.sexes)
        where.and_in("year",  args.years)

        # Find the total number of entries in the database matching each of the
        # criteria except for the name specification. This gives the denominator
        # for determining the frequency of a given name in the pool.
        cursor = conn.cursor()
        cursor.execute("SELECT SUM(count) as count FROM namesbystate" +
                       where.clause, where.args)
        total = cursor.fetchone()[0]

        # Find the total number of instances of each name matching all criteria.
        where.and_like("name", args.names)
        cursor.execute("SELECT name, SUM(count) as count FROM namesbystate" +
                       where.clause + " GROUP BY name ORDER BY count DESC",
                       where.args)

        for entry in reversed(cursor.fetchmany(args.display_limit)):
            print("{:<16}1:{}".format(entry[0], int(total / entry[1])))

def main():
    parser = argparse.ArgumentParser(prog="name_query")
    subparsers = parser.add_subparsers()
    create_parser = subparsers.add_parser("create")
    create_parser.add_argument("data_dir")
    create_parser.set_defaults(func=create_table)

    query_parser = subparsers.add_parser("query")
    query_parser.add_argument("--names", nargs="*")
    query_parser.add_argument("--states", nargs="*")
    query_parser.add_argument("--years", nargs="*")
    query_parser.add_argument("--sexes", nargs="*")
    query_parser.add_argument("--display_limit", type=int, default=100)
    query_parser.set_defaults(func=query_names)

    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
